//Mohammad Mahbub Rahman 2236383
package package2;
import package1.Bicycle;

public class BikeStore {
    public static void main (String[] args){

        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("BMX", 2, 30);
        bikes[1] = new Bicycle("FastBike", 4, 30);
        bikes[2] = new Bicycle("RaceBike", 6, 80);
        bikes[3] = new Bicycle("SlowBike", 1, 20);

        for (int i = 0; i < bikes.length; i++){
            System.out.println(bikes[i]);
        }


    }
    
}
